<?php
/**
 * @author Marceli Podstawski <marceli.podstawski@gmail.com>
 */

namespace MacPain\AssignProductsToMsi\Helper;

class Constants
{

    const ASSIGN_PRODUCTS_TO_MSI_COMMAND_NAME = 'macpain:products:assign_to_msi';
    const ASSIGN_PRODUCTS_TO_MSI_COMMAND_DESCRIPTION = 'Assign products to MSI by source code.';
    const ASSIGN_PRODUCTS_TO_MSI_SOURCE_OPTION_NAME = 'source_code';
    const ASSIGN_PRODUCTS_TO_MSI_SOURCE_OPTION_SHORTCUT = 's';
    const ASSIGN_PRODUCTS_TO_MSI_SOURCE_OPTION_DESCRIPTION = 'Add source code it is required value.';

}
