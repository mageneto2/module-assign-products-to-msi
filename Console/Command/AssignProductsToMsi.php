<?php
/**
 * @author Marceli Podstawski <marceli.podstawski@gmail.com>
 */

namespace MacPain\AssignProductsToMsi\Console\Command;

class AssignProductsToMsi extends \Symfony\Component\Console\Command\Command
{

    protected \Magento\Framework\App\State $appState;
    protected \Magento\InventoryCatalogApi\Api\DefaultSourceProviderInterface $defaultSourceProvider;
    protected \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory;
    protected \Magento\InventoryCatalogApi\Model\SourceItemsProcessorInterface $sourceItemsProcessor;

    public function __construct
    (
        \Magento\Framework\App\State $appState,
        \Magento\InventoryCatalogApi\Api\DefaultSourceProviderInterface $defaultSourceProvider,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\InventoryCatalogApi\Model\SourceItemsProcessorInterface $sourceItemsProcessor,
        string $name = null
    )
    {
        $this->appState = $appState;
        $this->defaultSourceProvider = $defaultSourceProvider;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->sourceItemsProcessor = $sourceItemsProcessor;

        parent::__construct($name);
    }

    protected function configure(): void
    {
        $this->setName(\MacPain\AssignProductsToMsi\Helper\Constants::ASSIGN_PRODUCTS_TO_MSI_COMMAND_NAME);
        $this->setDescription(\MacPain\AssignProductsToMsi\Helper\Constants::ASSIGN_PRODUCTS_TO_MSI_COMMAND_DESCRIPTION);
        $this->addOption(
            \MacPain\AssignProductsToMsi\Helper\Constants::ASSIGN_PRODUCTS_TO_MSI_SOURCE_OPTION_NAME,
            \MacPain\AssignProductsToMsi\Helper\Constants::ASSIGN_PRODUCTS_TO_MSI_SOURCE_OPTION_SHORTCUT,
            \Symfony\Component\Console\Input\InputOption::VALUE_REQUIRED,
            \MacPain\AssignProductsToMsi\Helper\Constants::ASSIGN_PRODUCTS_TO_MSI_SOURCE_OPTION_DESCRIPTION
        );

        parent::configure();
    }

    protected function execute(
        \Symfony\Component\Console\Input\InputInterface $input,
        \Symfony\Component\Console\Output\OutputInterface $output
    ): int
    {
        $this->appState->setAreaCode('frontend');
        $sourceCode = $input->getOption(\MacPain\AssignProductsToMsi\Helper\Constants::ASSIGN_PRODUCTS_TO_MSI_SOURCE_OPTION_NAME);
        if (empty($sourceCode)) {
            $output->writeln(sprintf(
                '<error>%s</error>',
                __('Missing source code.')
            ));

            $sourceCode = $this->defaultSourceProvider->getCode();
            $output->writeln(sprintf(
                '<info>%s</info>',
                __('Default source code - %1 added.', $sourceCode)
            ));
        }

        $productCollection = $this->productCollectionFactory->create();
        $productCollection->addAttributeToFilter('type_id', [
            'eq' => 'simple'
        ]);
        $items = $productCollection->getSize();
        $i = 1;
        $sourceItems = [];
        /** @var \Magento\Catalog\Model\Product $product */
        foreach ($productCollection as $product) {
            if (!$product->getSku()) {
                $output->writeln(sprintf(
                    '<error>%s</error>',
                    __('Product with ID %1 have empty SKU.', $product->getId())
                ));
                $i++;
                continue;
            }

            $output->writeln(sprintf(
                '<info>%s</info>',
                __('(%1-%2) Assigning stock source to product - %3.', $i, $items, $product->getSku())
            ));

            $data = [
                \Magento\InventoryApi\Api\Data\SourceItemInterface::SOURCE_CODE => $sourceCode,
                \Magento\InventoryApi\Api\Data\SourceItemInterface::STATUS => \Magento\InventoryApi\Api\Data\SourceItemInterface::STATUS_IN_STOCK,
                \Magento\InventoryApi\Api\Data\SourceItemInterface::QUANTITY => 999
            ];

            $this->sourceItemsProcessor->execute($product->getSku(), [$data]);

            $i++;
        }

        return 1;
    }

}
